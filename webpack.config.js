const path = require('path')
// const PUBLIC_PATH = 'https://timetable.ensar.me/'
const PUBLIC_PATH = '/'

const HtmlWebPackPlugin = require('html-webpack-plugin')
// const ManifestPlugin = require('webpack-manifest-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const WebpackPwaManifest = require('webpack-pwa-manifest')
const SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin')
// const HtmlWebpackPlugin = require('html-webpack-plugin')

const htmlWebpackPlugin = new HtmlWebPackPlugin({
  template: './src/index.html',
  filename: './index.html',
})
// const manifestPlugin = new ManifestPlugin()
const copyWebpackPlugin = new CopyWebpackPlugin([
  {
    from: 'src/favicon.ico',
    to: 'favicon.ico',
    toType: 'file',
  },
])
const webpackPwaManifest = new WebpackPwaManifest({
  name: 'Prayer Timetable Web App',
  short_name: 'Timetable',
  description: 'Prayer Timetable Web App!',
  orientation: 'portrait',
  display: 'standalone',
  includeDirectory: true,
  ios: true,
  inject: true,
  fingerprints: true,
  background_color: '#01579b',
  theme_color: '#01579b',
  'theme-color': '#01579b',
  start_url: '/',
  crossorigin: 'anonymous', // can be null, use-credentials or anonymous
  icons: [
    {
      // src: path.resolve('src/assets/icons/ios-icon.png'),
      src: `${__dirname}/src/styles/img/logo.png`,
      sizes: [120, 152, 167, 180, 1024],
      destination: path.join('icons', 'ios'),
      ios: true,
    },
    {
      // src: path.resolve('src/assets/icons/ios-icon.png'),
      src: `${__dirname}/src/styles/img/logo.png`,
      size: 1024,
      destination: path.join('icons', 'ios'),
      ios: 'startup',
    },
    {
      // src: path.resolve('src/assets/icons/android-icon.png'),
      src: `${__dirname}/src/styles/img/logo.png`,
      sizes: [36, 48, 72, 96, 144, 192, 512],
      destination: path.join('icons', 'android'),
    },
    // {
    //   // src: resolve('src/styles/img/logo.png'),
    //   src: __dirname + '/src/styles/img/logo.png',
    //   sizes: [96, 128, 192, 256, 384, 512], // multiple sizes
    // },
  ],
})
const sWPrecacheWebpackPlugin = new SWPrecacheWebpackPlugin({
  cacheId: 'my-domain-cache-id',
  dontCacheBustUrlsMatching: /\.\w{8}\./,
  filename: 'service-worker.js',
  minify: true,
  navigateFallback: `${PUBLIC_PATH}index.html`,
  staticFileGlobsIgnorePatterns: [/\.map$/, /manifest\.json$/],
})

// const htmlWebpackPlugin = new HtmlWebpackPlugin({
//   title: 'Prayer Timetable',
//   filename: 'index.html',
//   //   template: './src/index.html',
//   meta: {
//     viewport: 'width=device-width, initial-scale=1, shrink-to-fit=no',
//   },
// })

module.exports = {
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: 'style-loader',
          },
          {
            loader: 'css-loader',
            options: {
              modules: true,
              importLoaders: 1,
              localIdentName: '[name]_[local]_[hash:base64]',
              // sourceMap: true,
              minimize: true,
            },
          },
        ],
      },
      {
        test: /\.(jpg(2)?|jpeg|png|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: './images/',
            },
          },
        ],
      },
      {
        test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: './fonts/',
            },
          },
        ],
      },
    ],
  },
  resolve: {
    extensions: ['*', '.js', '.jsx'],
  },
  plugins: [htmlWebpackPlugin, copyWebpackPlugin, sWPrecacheWebpackPlugin, webpackPwaManifest],
  output: {
    // path: __dirname + '/dist',
    // // path: resolve('./'),
    // publicPath: '/',
    // filename: './dist/bundle.js',
    // path: __dirname + '/dist',
    // publicPath: './',
    // filename: 'bundle.js',
    // publicPath: PUBLIC_PATH,
  },
  devServer: {
    contentBase: './dist',
    // hot: true,
    port: 9000,
  },
}
