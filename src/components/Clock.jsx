import React from 'react'
import { connect } from 'react-redux'

import PropTypes from 'prop-types'
import styles from '../styles'

const ClockStyle = styles.clock

const mapStateToProps = (state) => ({ day: state.day })

const ConnectedClock = ({ day }) => (
  <ClockStyle>
    <div className="body">{day.now.format('HH:mm:ss')}</div>
  </ClockStyle>
)

const Clock = connect(
  mapStateToProps
  // mapDispatchToProps
)(ConnectedClock)
export default Clock

ConnectedClock.propTypes = {
  day: PropTypes.object,
}
