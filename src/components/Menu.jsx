import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import moment from 'moment-hijri'
import { push as Menu } from 'react-burger-menu'

import Refresh from 'react-feather/dist/icons/refresh-cw'
import Download from 'react-feather/dist/icons/download'
import Select from 'react-select'
import { switchOn, switchOff } from '../styles/img/icons'
import styles from '../styles'

import { getToggle, getMasjid } from '../store/actions/getSettings'

const options = [{ value: 'icci', label: 'ICCI' }, { value: 'ifi', label: 'IFI' }, { value: 'mai', label: 'MAI' }]

const setMasjid = (option) => {
  /* eslint-disable react/destructuring-assignment */
  // this.props.getMasjid(value)
  // setLocalMasjid(value)
  console.log(`Option selected:`, option)
  getMasjid(option)
}

const setToggle = (option) => {
  // this.setState({ selectedOption });
  console.log(`Option selected:`, option)
  getToggle(option)
  // return option
}

const jamaahShowDisp = (jamaahShow) => (
  <div
    className="switch"
    onClick={() => setToggle(!jamaahShow)}
    onKeyPress={() => setToggle(!jamaahShow)}
    role="button"
    tabIndex="0"
  >
    {jamaahShow ? switchOn : switchOff}
    <span>Show jamaah</span>
  </div>
)

const { MenuStyle } = styles

const updatedDisp = (settings) => {
  let result
  if (!settings.online) result = null
  // if < 1/1/2018 0:00
  else if (settings.updated < 1514764800) {
    result = (
      <div className="right">
        <Refresh size={18} />
        {' - '}
      </div>
    )
  } else {
    result = (
      <div className="right">
        <Refresh size={18} />
        {`${moment.duration(moment().diff(moment(settings.updated * 1000))).humanize()} ago`}
      </div>
    )
  }
  return result
}

// REFRESHDIFF
const refreshDiffDisp = (settings, downloaded) => {
  /* eslint-disable babel/no-unused-expressions */
  let diff
  downloaded !== '-' && moment.isMoment(downloaded)
    ? (diff = `${moment.duration(moment().diff(downloaded)).humanize()} ago (${settings.updateInterval})`)
    : (diff = '-')
  const result = settings.online ? (
    <div className="right">
      <Download size={18} />
      {diff}
    </div>
  ) : null

  return result
}

// MAPPING
const mapStateToProps = (state) => ({
  day: state.day,
  settings: state.settings,
  update: state.update,
})
// const mapDispatchToProps = (dispatch) => ({
//   getToggle: (toggle) => dispatch(setToggle(toggle)),
//   setMasjid: (select) => dispatch(setMasjid(select)),
//   // setLocal: (toggle) => dispatch(setLocal(toggle)),
// })

// COMPONENT
const ConnectedMenu = ({ settings, update }) => (
  <MenuStyle>
    <Menu
      width="70%"
      pageWrapId="main"
      outerContainerId="index"
      // isOpen
    >
      <div>Settings</div>
      <div>
        {jamaahShowDisp(settings.jamaahShow)}
        <br />
        {/* {masjidDisplay} */}
        {/* {`js: ${jamaahShow}`} */}
        {/* <a id="home" className="menu-item" href="/">
        Home
      </a> */}
      </div>
      <Select value={settings.masjid} onChange={setMasjid} options={options} />
      <div>
        {refreshDiffDisp(settings, update.downloaded)}
        {updatedDisp(settings)}
      </div>
    </Menu>
  </MenuStyle>
)

// EXPORT
const MenuComponent = connect(
  mapStateToProps
  // mapDispatchToProps
)(ConnectedMenu)
export default MenuComponent

ConnectedMenu.propTypes = {
  // day: PropTypes.object,
  update: PropTypes.object,
  settings: PropTypes.object,
}
