import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import styles from '../styles'

// import { down, up, circle, circleBg } from '../helpers/svg'
import { appendZero } from '../helpers/func'

const CountdownStyle = styles.countdown

const mapStateToProps = (state) => ({ prayers: state.prayers, day: state.day })

const ConnectedCountdown = ({ prayers }) => (
  <CountdownStyle percentage={prayers.percentage}>
    <div>{prayers.countDown.name}</div>
    <div className="countdown">
      {`${prayers.countDown.duration.hours()}:${appendZero(prayers.countDown.duration.minutes())}:${appendZero(
        prayers.countDown.duration.seconds()
      )}`}
    </div>
  </CountdownStyle>
)
const Countdown = connect(mapStateToProps)(ConnectedCountdown)
export default Countdown

ConnectedCountdown.propTypes = {
  prayers: PropTypes.object,
}
