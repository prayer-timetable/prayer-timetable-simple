/* eslint-disable babel/semi */
import React, { Component } from 'react'
// import PropTypes from 'prop-types'
import { Helmet } from 'react-helmet'

class Head extends Component {
  constructor(props) {
    super(props)

    this.state = {}
  }

  render() {
    return (
      <Helmet>
        {/* <meta charSet="utf-8" /> */}
        {/* <title>My Title</title> */}
        {/* <link rel="canonical" href="http://mysite.com/example" /> */}
        <link rel="apple-touch-icon" sizes="57x57" href="/static/favicons/apple-icon-57x57.png" />
        <link rel="apple-touch-icon" sizes="60x60" href="/static/favicons/apple-icon-60x60.png" />
        <link rel="apple-touch-icon" sizes="72x72" href="/static/favicons/apple-icon-72x72.png" />
        <link rel="apple-touch-icon" sizes="76x76" href="/static/favicons/apple-icon-76x76.png" />
        <link rel="apple-touch-icon" sizes="114x114" href="/static/favicons/apple-icon-114x114.png" />
        <link rel="apple-touch-icon" sizes="120x120" href="/static/favicons/apple-icon-120x120.png" />
        <link rel="apple-touch-icon" sizes="144x144" href="/static/favicons/apple-icon-144x144.png" />
        <link rel="apple-touch-icon" sizes="152x152" href="/static/favicons/apple-icon-152x152.png" />
        <link rel="apple-touch-icon" sizes="180x180" href="/static/favicons/apple-icon-180x180.png" />
        <link rel="icon" type="image/png" sizes="192x192" href="/static/favicons/android-icon-192x192.png" />
        <link rel="icon" type="image/png" sizes="32x32" href="/static/favicons/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="96x96" href="/static/favicons/favicon-96x96.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="/static/favicons/favicon-16x16.png" />
        <meta name="msapplication-TileColor" content="#2f4f4f" />
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png" />
        <meta name="theme-color" content="#2f4f4f" />
        {/* <link rel="manifest" href="/static/manifest/manifest.webmanifest" /> */}
        {/* <link rel="manifest" href="/static/favicons/manifest.json" /> */}
        {/* <meta title="" /> */}
      </Helmet>
    )
  }
}

export default Head

// Countdown.propTypes = {
//   prayers: PropTypes.shape({}),
// }
// Countdown.defaultProps = {
//   prayers: {},
// };
