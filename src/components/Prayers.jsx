import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import styles from '../styles'

const PrayersStyle = styles.prayers
const PrayerStyle = styles.prayer

const mapStateToProps = (state) => ({ prayers: state.prayers, settings: state.settings })

const renderPrayers = (settings, prayers) => {
  const { next, isAfterIsha, focus } = prayers
  const { join, jamaahShow } = settings

  const prayerList = isAfterIsha ? prayers.prayers.tomorrow : prayers.prayers.today

  const isNext = (prayer) => (jamaahShow ? !!(focus.name === prayer.name) : !!(next.name === prayer.name))

  const adhan = (prayer) => {
    let result
    if (jamaahShow && (join === '1' || join === true) && prayer.name === 'isha') {
      result = <div className="adhanTime">after</div>
    } else {
      result = <div className="adhanTime right">{prayer.time.format('H:mm')}</div>
    }
    return result
  }

  const iqamah = (prayer) => {
    let result
    if (jamaahShow && (join === '1' || join === true) && prayer.name === 'isha') {
      result = <div className="iqamahTime">maghrib</div>
    } else if (jamaahShow && prayer.name !== 'shurooq') {
      result = (
        <div className={`iqamahTime${prayer.isJamaahPending ? ' pending' : ''}`}>{prayer.jtime.format('H:mm')}</div>
      )
    } else if (jamaahShow && prayer.name === 'shurooq') {
      result = <div className="iqamahTime" />
    } else {
      result = ''
    }
    return result
  }

  /* eslint-disable react/no-array-index-key */
  return prayerList.map((prayer, index) => (
    <PrayerStyle key={index} prayer={prayer} next={isNext(prayer)}>
      {/* {console.log(prayers.next.name, isNext(prayer))} */}
      <div className="prayerName">{prayer.name}</div>
      {adhan(prayer)}
      {iqamah(prayer)}
    </PrayerStyle>
  ))
}

const ConnectedPrayers = ({ settings, prayers }) => (
  <PrayersStyle>
    <div className="prayerHeader">
      <div className="prayerName">Prayer</div>
      <div>Adhan</div>
      {settings.jamaahShow ? <div>{settings.masjid.label}</div> : null}
    </div>
    {renderPrayers(settings, prayers)}
  </PrayersStyle>
)
const Prayers = connect(mapStateToProps)(ConnectedPrayers)
export default Prayers

ConnectedPrayers.propTypes = {
  settings: PropTypes.object,
  prayers: PropTypes.object,
}
