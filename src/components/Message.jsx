import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import styles from '../styles'

const MessageStyle = styles.message

const mapStateToProps = (state) => ({ settings: state.settings })

const ConnectedMessage = ({ settings }) => (
  <MessageStyle>
    {settings.announcement ? <h3>{settings.announcement}</h3> : ''}
    {settings.text.ar ? <div className="ar">{settings.text.ar}</div> : null}
    {settings.text.en ? <div className="en">{settings.text.en}</div> : null}
  </MessageStyle>
)
const Message = connect(mapStateToProps)(ConnectedMessage)
export default Message

ConnectedMessage.propTypes = {
  settings: PropTypes.object,
}
