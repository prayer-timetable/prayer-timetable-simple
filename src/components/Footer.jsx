import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import moment from 'moment-hijri'

import { Offline, Online } from 'react-detect-offline'
import styles from '../styles'

import { wifiOn, wifiOff } from '../styles/img/icons'

const FooterStyle = styles.footer

// RAMADAN
const ramadanDisp = (day) => {
  const result = day.ramadanCountdown ? <div className="left">{`${day.ramadanCountdown} to Ramadan`}</div> : null
  return result
}

// JUMMUAH
const jummuahDisp = (settings) => {
  const jummuahTime = moment({
    hour: settings.jummuahtime[0],
    minute: settings.jummuahtime[1],
  }).day(5)

  const result = settings.jummuahtime[0] ? <div className="center">{`Jummuah ${jummuahTime.format('H:mm')}`}</div> : 0
  return result
}

// TARAWEEH
const taraweehDisp = (settings) => {
  const taraweehTime = moment({
    hour: settings.taraweehtime[0],
    minute: settings.taraweehtime[1],
  })

  const result =
    moment().format('iM') === '9' ? (
      <div className="left">{`${settings.labels.taraweeh} ${taraweehTime.format('H:mm')}`}</div>
    ) : null

  return result
}

// ONLINE
const onlineDisp = (settings) => {
  const result = settings.online ? (
    <div className="wifi">
      <Offline>{wifiOff}</Offline>
      <Online>{wifiOn}</Online>
    </div>
  ) : null
  return result
}

// MAPPING
const mapStateToProps = (state) => ({
  day: state.day,
  settings: state.settings,
  update: state.update,
})

// COMPONENT
const ConnectedFooter = ({ day, settings }) => (
  <FooterStyle>
    {onlineDisp(settings)}
    {jummuahDisp(settings)}
    {ramadanDisp(day)}
    {taraweehDisp(settings)}
  </FooterStyle>
)

// EXPORT
const Footer = connect(mapStateToProps)(ConnectedFooter)
export default Footer

ConnectedFooter.propTypes = {
  day: PropTypes.object,
  settings: PropTypes.object,
}
