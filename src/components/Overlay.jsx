import React from 'react'
import { connect } from 'react-redux'

import styles from '../styles'
import logo from '../config/logo.svg'

const OverlayStyle = styles.overlay

const logoImg = <img src={logo} alt="logo" />

const mapStateToProps = (state) => ({ day: state.day, settings: state.settings })

const ConnectedOverlay = ({ day, settings }) => {
  const output = day.overlayActive ? (
    <OverlayStyle>
      <div className="logo">{logoImg}</div>
      <div>
        <h1>{day.overlayTitle}</h1>
        <div>{day.now.format('ddd D MMMM YYYY')}</div>
        <div>{day.hijri.format('iD iMMMM YYYY')}</div>
      </div>
      <div className="title">{settings.title}</div>
    </OverlayStyle>
  ) : (
    ''
  )
  return output
}
const Overlay = connect(mapStateToProps)(ConnectedOverlay)
export default Overlay
