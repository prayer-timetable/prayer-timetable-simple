import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import styles from '../styles'
import logoicci from '../config/icci/logo-icci.svg'
import logoifi from '../config/ifi/logo-ifi.svg'
import logomai from '../config/mai/logo-mai.svg'

const HeaderStyle = styles.header

const logoDisp = (option) => {
  switch (option) {
    case 'icci':
      return <img src={logoicci} alt="logo" />
    case 'ifi':
      return <img src={logoifi} alt="logo" />
    case 'mai':
      return <img src={logomai} alt="logo" />
    default:
      return ''
  }
}

const mapStateToProps = (state) => ({ day: state.day, settings: state.settings })

const ConnectedHeader = ({ day, settings }) => (
  <HeaderStyle>
    <div>{day.now.format('ddd DD MMM YYYY')}</div>
    <div>{day.hijri.format('iDD iMMM iYYYY')}</div>
    <div className="logo">{logoDisp(settings.masjid.value)}</div>
    {/* <div className="center">{settings.title}</div> */}
  </HeaderStyle>
)
const Header = connect(mapStateToProps)(ConnectedHeader)
export default Header

ConnectedHeader.propTypes = {
  day: PropTypes.object,
  settings: PropTypes.object,
}
