/* eslint-disable */

import React, { Component } from 'react'
import Menu from './components/Menu'

import { connect } from 'react-redux'

// import PropTypes from 'prop-types'

// import Head from './components/Head'
// import Overlay from './components/Overlay'
import Header from './components/Header'
import Clock from './components/Clock'
import Prayers from './components/Prayers'
import Countdown from './components/Countdown'
// import Message from './components/Message'
import Footer from './components/Footer'

// import Counter from './components/Counter'
// import List from './components/List'
// import Form from './components/Form'

import styles from './styles'

// import { tick, update } from './helpers/update'

const { AppStyle, GlobalStyle } = styles

class TimetableApp extends Component {
  constructor(props) {
    super(props)

    this.state = {}
  }

  /* *********************************************************************
  RENDERING
  ********************************************************************* */
  render() {
    // console.log('MAIN PROPS', this.props)

    // let overlay
    // if (overlayActive) {
    //   overlay = <Overlay settings={settings} day={day} overlayTitle={overlayTitle} />
    // } else overlay = null

    return (
      <AppStyle>
        {/* <Head /> */}
        {/* {overlay} */}
        {/* <Counter /> */}
        {/* <List /> */}
        <Menu />
        <section id="main" style={{ height: '100%', width: '100%' }}>
          <Header />
          <Clock />
          {/* <Countdown prayers={prayers} /> */}
          <Prayers />
          <Countdown />
          {/* <Message /> */}
          <Footer />
          {/* <Overlay /> */}
        </section>
        <GlobalStyle />
      </AppStyle>
    )
  }
}

const ConnectedTimetableApp = connect(
  null
  // mapDispatchToProps
)(TimetableApp)

export default ConnectedTimetableApp
export { TimetableApp }
