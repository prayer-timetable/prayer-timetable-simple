/*eslint-disable*/

const wordCount = (str) => str.split(' ').filter((n) => n !== '').length

const appendZero = (unit) => {
  if (unit < 10) return `0${unit}`
  return `${unit}`
}

export default { wordCount }
export { appendZero, wordCount }
