/*eslint-disable*/

const tekstPlural = (input) => {
  let result
  if (input > 10 && input < 20) result = 'tekstova'
  else {
    switch (input % 10) {
      case 1:
        result = 'tekst'
        break
      case 2:
      case 3:
      case 4:
        result = 'teksta'
        break
      default:
        result = 'tekstova'
    }
  }
  // console.log(input % 10)
  return result
}

const autorPlural = (input) => {
  let result
  if (input > 10 && input < 20) result = 'autora'
  else {
    switch (input % 10) {
      case 1:
        result = 'autor'
        break
      case 2:
      case 3:
      case 4:
        result = 'autora'
        break
      default:
        result = 'autora'
    }
  }
  // console.log(input % 10)
  return result
}

const brojPlural = (input) => {
  let result
  if (input > 10 && input < 20) result = 'brojeva'
  else {
    switch (input % 10) {
      case 1:
        result = 'broj'
        break
      case 2:
      case 3:
      case 4:
        result = 'broja'
        break
      default:
        result = 'brojeva'
    }
  }
  // console.log(input % 10)
  return result
}

const temaPlural = (input) => {
  let result
  if (input > 10 && input < 20) result = 'tema'
  else {
    switch (input % 10) {
      case 1:
        result = 'tema'
        break
      case 2:
      case 3:
      case 4:
        result = 'teme'
        break
      default:
        result = 'tema'
    }
  }
  // console.log(input % 10)
  return result
}

const minutPlural = (input) => {
  let result
  if (input > 10 && input < 20) result = 'minuta'
  else {
    switch (input % 10) {
      case 1:
        result = 'minut'
        break
      case 2:
      case 3:
      case 4:
        result = 'minute'
        break
      default:
        result = 'minuta'
    }
  }
  // console.log(input % 10)
  return result
}

const godinaPlural = (input) => {
  let result
  if (input > 10 && input < 20) result = 'godina'
  else {
    switch (input % 10) {
      case 1:
        result = 'godina'
        break
      case 2:
      case 3:
      case 4:
        result = 'godine'
        break
      default:
        result = 'godina'
    }
  }
  // console.log(input % 10)
  return result
}

export default { tekstPlural, autorPlural, brojPlural, temaPlural }
export { tekstPlural, autorPlural, brojPlural, temaPlural, minutPlural, godinaPlural }
