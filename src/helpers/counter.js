const wordCount = (str) => str.split(' ').filter((n) => n !== '').length

export default { wordCount }
export { wordCount }
