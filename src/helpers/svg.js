import React from 'react'

const down = (
  <svg viewBox="0 0 100 50" className="down">
    <g transform="matrix(1,0,0,1,-55.5108,-35.5108)">
      <g transform="matrix(0.5,-0,0,0.428571,30,20)">
        <path d="M60,50L150,80L240,50" className="line" />
      </g>
      <g transform="matrix(0.5,-0,0,0.428571,30,30)">
        <path d="M60,50L150,80L240,50" className="line" />
      </g>
    </g>
  </svg>
)

const up = (
  <svg viewBox="0 0 100 50" className="up">
    <g transform="matrix(1,0,0,1,-55.5108,-35.5108)">
      <g transform="matrix(0.5,0,0,-0.428571,30,85)">
        <path d="M60,50L150,80L240,50" className="line" />
      </g>
      <g transform="matrix(0.5,0,0,-0.428571,30,95)">
        <path d="M60,50L150,80L240,50" className="line" />
      </g>
    </g>
  </svg>
)

// const percOut = `${percentage}, 100vw`

const circle = (
  <svg viewBox="0 0 36 36" className="circular-chart">
    <path
      className="circle"
      // strokeDasharray={`${times.percentage}, 100`}
      // strokeDasharray="76.5, 100"
      // strokeDasharray={percOut}
      // strokeDasharray={`${percentage}, 100`}
      d="M18 2.0845
      a 15.9155 15.9155 0 0 1 0 31.831
      a 15.9155 15.9155 0 0 1 0 -31.831"
    />
  </svg>
)

const circleBg = (
  <svg viewBox="0 0 36 36" className="circular-bg">
    <path
      className="circle"
      // strokeDasharray={`${times.percentage}, 100`}
      // strokeDasharray="76.5, 100"
      // strokeDasharray={percOut}
      // strokeDasharray={`${percentage}, 100`}
      d="M18 2.0845
      a 15.9155 15.9155 0 0 1 0 31.831
      a 15.9155 15.9155 0 0 1 0 -31.831"
    />
  </svg>
)

export { down, up, circle, circleBg }
