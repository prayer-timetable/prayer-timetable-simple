import localforage from 'localforage'
import store from '../index'
import { GET_TOGGLE, GET_MASJID } from '../constants/actionTypes'
import { getUpdate } from './getUpdate'
// console.log('STORE', store.getState())
// const { log } = store.getState().settings

// const setLocalJamaah = async (toggle) => {
//   log && console.log(toggle)
//   await localforage.setItem('settings', { ...store.getState().settings, jamaahShow: toggle })
//   // console.log('GETTING LF', await localforage.getItem('settings'))
//   // console.log('whats set?', { ...store.getState().settings, jamaahShow: toggle })
// }

// const setLocalMasjid = async (select) => {
//   log && console.log(select)
//   await localforage.setItem('settings', { ...store.getState().settings, masjid: select })
//   await getUpdate(true)
//   // console.log('GETTING LF', await localforage.getItem('settings'))
//   // console.log('whats set?', { ...store.getState().settings, jamaahShow: toggle })
// }

const getToggle = async (toggle) => {
  console.log('called toggle')
  await localforage.setItem('settings', { ...store.getState().settings, jamaahShow: toggle })
  store.dispatch({
    type: GET_TOGGLE,
    toggle,
  })
}

const getMasjid = async (select) => {
  console.log('called masjid', select)
  await getUpdate(true, select.value)
  await localforage.setItem('settings', { ...store.getState().settings, masjid: select })
  store.dispatch({
    type: GET_MASJID,
    masjid: select,
  })
}

export default getToggle
export { getToggle, getMasjid }
