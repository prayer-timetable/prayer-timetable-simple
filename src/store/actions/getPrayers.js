import { prayersCalc } from 'prayer-timetable-lib'
// import { prayersCalc } from '../../helpers/test_calc' // testing purposes

import store from '../index'
import { GET_PRAYERS } from '../constants/actionTypes'

const getPrayers = () => {
  // console.log('GETPRAYERS', store.getState().settings)
  const { prayers, percentage, countUp, countDown, isAfterIsha, next, focus } = prayersCalc(
    store.getState().timetable,
    store.getState().settings,
    store.getState().settings.jamaahShow
  )
  // console.log('called getprayers', store.getState().settings.jamaahShow)

  return store.dispatch({
    type: GET_PRAYERS,
    prayers,
    percentage,
    countUp,
    countDown,
    isAfterIsha,
    next,
    focus,
  })
}

export default getPrayers
