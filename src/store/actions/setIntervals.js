import { getUpdate } from './getUpdate'
import getDay from './getDay'
import getPrayers from './getPrayers'

// RUN
const run = async () => {
  await getDay()
  getPrayers()
}
// TICK
/* eslint-disable no-use-before-define */
const tick = async () => {
  await clearInterval(timer)
  const timer = setInterval(() => run(), 1000)
}

const update = async (interval = 1) => {
  getUpdate()
  /* eslint-disable no-use-before-define */
  await clearInterval(updatetimer)
  let updatetimer = setInterval(() => getUpdate(), 1000 * 60 * interval)
}

export { tick, update }
