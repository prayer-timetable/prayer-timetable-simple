export const GET_TIMETABLE = 'GET_TIMETABLE'
export const GET_TOGGLE = 'GET_TOGGLE'
export const GET_MASJID = 'GET_MASJID'

export const GET_UPDATE = 'GET_UPDATE'
export const GET_LOCAL = 'GET_LOCAL'

export const GET_PRAYERS = 'GET_PRAYERS'
export const GET_DAY = 'GET_DAY'

// export default ADD_ARTICLE
