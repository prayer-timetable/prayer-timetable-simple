import { GET_TOGGLE, GET_UPDATE, GET_LOCAL, GET_MASJID } from '../constants/actionTypes'
import initialState from './initialState'
// import setPrayers from '../actions/setPrayers'

const settingsReducer = (state = initialState.settings, action) => {
  // console.log(action)

  switch (action.type) {
    case GET_TOGGLE:
      return { ...state, jamaahShow: action.toggle }

    case GET_UPDATE:
      // console.log('GET_UPDATE from SettingsREDUCER', state, action)
      return {
        ...state,
        // ...action.payload.settings,
        ...action.payload.settings,
      } // merge

    case GET_LOCAL:
      // console.log('GET_LOCAL from SettingsREDUCER', state, action)
      return {
        ...state,
        ...action.payload.settings,
      } // merge

    case GET_MASJID:
      // console.log('GET_MASJID from SettingsREDUCER', state, action)
      return {
        ...state,
        masjid: action.masjid,
      }
    default:
      return state
  }
}

export default settingsReducer
