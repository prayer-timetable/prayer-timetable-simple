import { GET_DAY } from '../constants/actionTypes'
import initialState from './initialState'

const dayReducer = (state = initialState.day, action) => {
  // console.log('PRAYERSREDUCER', state, action.payload)
  switch (action.type) {
    case GET_DAY:
      // console.log('GET_DAY', action)
      // return state.concat([action.payload])
      return {
        ...state,
        now: action.now,
        hijri: action.hijri,
        overlayActive: action.overlayActive,
        overlayTitle: action.overlayTitle,
        // ramadanCountdown: action.day.ramadanCountdown,
      }
    default:
      return state
  }
}

export default dayReducer
