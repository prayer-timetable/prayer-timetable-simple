/* eslint-disable babel/semi */
/* eslint-disable babel/no-unused-expressions */
/* eslint-disable no-console */
/* eslint-disable arrow-body-style */
/* eslint-disable arrow-parens */
/* eslint-disable no-confusing-arrow */
/* eslint-disable implicit-arrow-linebreak */
/* eslint-disable indent */
import styled, { createGlobalStyle, css } from 'styled-components'

import './normalize.css'
// import './fonts/open-sans.css'
import './fonts/cairo.css' // Cairo:200,300,400,600,700,900&amp;subset=arabic,latin-ext

import theme from '../config/theme.json'

const animations = {
  animationCurveFastOutSlowIn: 'cubic-bezier(0.4, 0, 0.2, 1)',
  animationCurveLinearOutSlowIn: 'cubic-bezier(0, 0, 0.2, 1)',
  animationCurveFastOutLinearIn: 'cubic-bezier(0.4, 0, 1, 1)',
  animationCurveDefault: 'cubic-bezier(0.4, 0, 0.2, 1)',
  animationSpeedDefault: '250ms',
  animationSpeedFast: '200ms',
  animationSpeedSlow: '300ms',
}

const componentStyle = (text, background, border) => {
  return `
      color: ${text.color};
      font-weight: ${text.weight};
      font-size: ${text.size};
      font-family: ${text.family};
      // background-color: ${background.color};
      background-image: ${background.type}-gradient(${background.color},
        ${background.color2});
      ${background.image ? `background-image: url(${background.image})` : ''};
      border-width: ${border.width};
      border-style: ${border.style};
      border-color: ${border.color};
    `
}

const defaultComponentStyle = theme.main || {
  text: {
    color: 'rgba(30,30,30,1)',
    color2: 'rgba(30,30,30,1)',
    size: '24px',
    size2: '24px',
    weight: 400,
    weight2: 400,
    family: 'Cairo',
  },
  background: {
    color: 'rgba(30,30,30,0)',
    color2: 'rgba(30,30,30,0)',
    type: 'linear',
  },
  border: {
    width: '0',
    style: 'solid',
    color: 'rgba(100,175,100,1)',
  },
}
/* eslint-disable object-curly-newline */
const {
  main = defaultComponentStyle,
  header = defaultComponentStyle,
  clock = defaultComponentStyle,
  prayers = defaultComponentStyle,
  prayer = defaultComponentStyle,
  countdown = defaultComponentStyle,
  message = defaultComponentStyle,
  footer = defaultComponentStyle,
  overlay = defaultComponentStyle,
} = theme
// const { header, main, clock, prayers, prayer, countdown, message, footer } = theme

const flex = (direction = 'row') => {
  return `
      display: flex;
      box-sizing: border-box;
      flex-basis: auto; //0; 200px;
      justify-content: space-between;
      align-items: center;
      // width: 100%;
      // width: calc(100% - 2rem);
      // height: 100%;
      flex-direction: ${direction};
    `
}

export default {
  GlobalStyle: createGlobalStyle`
    * {
      margin: 0;
      padding: 0;
      box-sizing: border-box;
    }
    html, body, #root, #index {
      height: 100%;
    }
    strong {
      font-weight: 600;
      color: #000;
    }
    #root, #index {
      ${componentStyle(main.text, main.background, main.border)} // style
    }
    body {
      background: ${main.background.color};
      color: ${main.text.color};
      font: ${main.text.family}, 'Geneva', 'sans-serif';
    }
  
    h1, h2, h3, h4, h5, h6{
      font-family: 'Cairo', 'Oswald', 'Geneva', 'sans-serif';
      font-weight: 700;
      margin: 0;
      padding: 0;
      line-height: 1.25;
    }
    p, ul, li {
      margin: 0;
      padding: 0;
      line-height: 1.2;
    }
  `,
  animations,
  // color,
  // font,

  AppStyle: styled.section`
    ${flex('column')}
    width: 100%;
    height: 100%;
  `,

  header: styled.div`
    ${flex()} width: 100%;
    font-size: ${header.text.size};
    height: 5%;
    ${componentStyle(header.text, header.background, header.border)} // other
    padding: .25rem 1rem .25rem 2.5rem;
    div {
      ${flex()}
      width: 45%;

      :last-child {
        justify-content: flex-end;
        text-align: right;
      }
    }
    & .logo {
      justify-content: flex-end;
      width: 10%;
      height: 1.2rem;
      img {
        height: 1.2rem;
        padding-right: 0.25rem;
      }
    }
  `,

  clock: styled.section`
    ${flex()}
    justify-content: center;
    width: 100%;
    height: 20%;
    ${componentStyle(clock.text, clock.background, clock.border)} // style

    & .body {
      ${flex('column')} .timeRow {
        font-size: ${clock.text.size};
        font-weight: ${clock.text.weight};
      }
      .dateRow {
        font-size: ${clock.text.size2};
        font-weight: ${clock.text.weight2};
        text-align: center;
      }
    }
  `,

  prayers: styled.section`
    ${flex('column')} height: 60%;
    width: 100%;
    ${componentStyle(prayers.text, prayers.background, prayers.border)} // style


    .prayerHeader {
      ${flex()} width: 100%;
      padding: 0.25rem 1rem;
      // flex: .2;
      height: 100%;
      border-width: ${prayers.border.width};
      border-style: ${prayers.border.style};
      border-color: ${prayers.border.color};
    }
  `,

  prayer: styled.div`
    ${flex()} width: 100%;
    text-transform: uppercase;
    height: 100%;
    padding: 0.125rem 1rem;
    ${componentStyle(prayer.text, prayer.background, prayer.border)} // style
    background: ${prayer.background.color};
    :last-child {
      border: none;
    }
    // padding: 1rem;
    div {
      width: 100%;
      :nth-child(2n) {
        text-align: center;
      }
      :first-child {
        text-align: left;
      }
      :last-child {
        text-align: right;
      }
    }
    & .pending {
      :before {
        content: '⇒ ';
      }
    }
    ${(props) =>
      props.next
        ? `background: ${prayer.background.color2};color: ${prayer.text.color2};font-weight: ${prayer.text.weight2};`
        : ''};
  `,

  countdown: styled.div`
    ${flex()} // flex
    width: 100%;
    ${componentStyle(countdown.text, countdown.background, countdown.border)} // style
    justify-content: center;
    height: 10%;
    div {
      padding: 0 0.5rem;
      z-index: 1;
      margin-bottom: 1%;
    }
    position: relative;

    ${(props) =>
      props.percentage
        ? `
    :before {
      z-index: 0;
      content: '';
      position: absolute;
      bottom: 0; left: 0;
      background: ${countdown.border.color};
      width: ${props.percentage}%;
      height: 10%;
      // border-bottom: 5px solid #999;
    }
    `
        : ''};
  `,

  message: styled.div`
    ${flex('column')} justify-content: space-around;
    height: 18%;
    padding: 0.1rem 1rem;
    width: 100%;
    ${componentStyle(message.text, message.background, message.border)} // style


    div {
      margin: 0;
    }
    .en {
      font-size: ${message.text.size};
      font-weight: ${message.text.weight};
      text-align: left;
    }
    .ar {
      font-size: ${message.text.size2};
      font-weight: ${message.text.weight2};
      text-align: right;
    }
  `,

  footer: styled.div`
    ${flex()} width: 100%;
    height: 5%;
    ${componentStyle(footer.text, footer.background, footer.border)} // style
    padding: 0.25rem 0.5rem;
    div {
      ${flex()}
      justify-content: center;
      svg {
        padding-right: 0.75rem;
      }
      :first-child {
        justify-content: flex-start;
      }
      :last-child {
        justify-content: flex-end;
      }
    }
    .wifi {
      svg {
        // width: 0.75rem;
        height: 0.8rem;
        .on {
          fill: ${footer.text.color};
        }
        .off {
          fill: ${footer.text.color2};
        }
      }
    }
  `,

  MenuStyle: styled.div`
    /* Position and sizing of burger button */
    .bm-burger-button {
      position: fixed;
      width: 18px;
      height: 18px;
      left: 4px;
      top: 4px;
    }

    /* Color/shape of burger icon bars */
    .bm-burger-bars {
      background: #373a47;
    }

    /* Position and sizing of clickable cross button */
    .bm-cross-button {
      height: 24px;
      width: 24px;
    }

    /* Color/shape of close button cross */
    .bm-cross {
      background: #bdc3c7;
    }

    & .bm-menu-wrap {
      // color: ${main.color2};
      // width: 200px !important;
      // background: ${main.background};
      // width: 100%;
      top: 0;
      left: 0;
    }
    /* General sidebar styles */
    .bm-menu {
      // width: 100%;
      background: ${main.text.color2};
      padding: .25em 1.5em 0;
      font-size: 1.15em;
    }

    /* Morph shape necessary with bubble or elastic */
    .bm-morph-shape {
      fill: #373a47;
    }

    /* Wrapper for item list */
    .bm-item-list {
      ${flex('column')}
      // justify-content: center;
      width: 100%;
      color: #b8b7ad;
      padding: 0.8em;
    }

    /* Individual item */
    .bm-item {
      ${flex()}

      width: 100%;
      display: inline-block;
      // display: flex!important;
      div {
        ${flex()}
        justify-content: flex-end;
        svg {
          margin-right: .5rem;
        }
      }
    }

    /* Styling of overlay */
    .bm-overlay {
      background: rgba(0, 0, 0, 0.3);
    }

    & .switch {
      // width: 20%;
      ${flex()}
      justify-content: flex-start;
      // padding-top: 0.2rem;
      // width: 2rem;
      svg {
        margin-right: 1rem;
        width: 3.2rem;
        height: 1.6rem;
      }
      .switchOn,
      .switchOff {
        fill-rule: evenodd;
        clip-rule: evenodd;
        stroke-linejoin: round;
        stroke-miterlimit: 1.32;

        .lite {
          fill: ${main.background.color};
          fill-rule: nonzero;
        }
        .dark {
          fill: ${main.background.color2};
          fill-rule: nonzero;
        }
      }
      .switchOn {
        .text {
          // font-family: 'ArialMT', 'Arial', sans-serif;
          font-size: 141.331px;
          fill: ${main.text.color};
        }
      }
      .switchOff {
        .text {
          // font-family: 'ArialMT', 'Arial', sans-serif;
          font-size: 141.331px;
          fill: ${main.background.color};
        }
      }
    }
  `,
  MenuStyle2: styled.div`
    position: absolute;
    // > div {
    //   margin: 5px;
    // }
    // top: 5px;
    // left: 5px;
    & .bm-overlay {
      // height: 100% !important;
      // width: 100% !important;
      background: ${main.transBlack50} !important;
    }
    & .bm-menu-wrap {
      color: ${main.color2};
      width: 200px !important;
      background: ${main.transBlack50};
      top: 0;
      left: 0;
      & .bm-menu {
        width: 100%;
        // padding: 1rem;
        & .bm-item-list {
          ${flex('column')} width: 100%;
          padding: 1rem;
          align-items: flex-start;
          div {
            // width: 100%;
          }
        }
      }
    }

    & .bm-cross-button {
      // z-index: 1000;
      & .bm-cross {
        background: ${main.background};
      }
    }

    & .bm-burger-button {
      display: block;
      // background: #ccc;
      // padding: .25rem !important;

      width: 24px !important;
      height: 14px !important;
      > span {
        width: 24px;
        display: block;
      }
      & .bm-icon {
        background: ${main.primaryDark};
        // width: 20px;
      }
      & .bm-burger-bars {
        margin: 5px;
        height: 2px !important;
        background: ${theme.dark ? main.primaryShade : main.primaryShine};
      }
      // button {
      //   // background: ${main.primaryDark};

      //   width: 1rem !important;
      //   height: 2rem !important;
      // }
    }


`,

  overlay: styled.div`
    ${flex('column')} justify-content: space-between;
    z-index: 999;

    color: ${overlay.text.color};
    background: ${overlay.background.color};

    flex: 0.175;
    position: fixed;
    padding: 2rem 1rem;
    width: 100%;
    height: 100%;
    font-size: 125%;
    div {
      margin: 0;
      text-align: center;
    }
    & .title {
      font-size: 75%;
    }
    h1 {
      color: ${overlay.text.color2};
      font-size: 150%;
      padding-bottom: 2rem;
    }
    & .logo {
      justify-content: center;
      width: 10%;
      height: 10rem;
      img {
        height: 10rem;
        padding-right: 0.25rem;
      }
    }
  `,

  main: css`
    color: ${main.text.color};
    margin-left: 1rem;
    display: inline-block;
    // float: right;
  `,

  light: css`
    color: ${main.text.color2};
  `,

  uppercase: css`
    text-transform: lowercase;
    font-size: 90%;
    font-weight: 500;
  `,

  smallText: css`
    font-size: 85%;
    a {
      padding: 0.25rem 0;
    }
  `,
}
// console.log(colors)
